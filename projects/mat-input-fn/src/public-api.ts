/*
 * Public API Surface of mat-input-fn
 */

export * from './lib/services/mat-input-fn.service';
export * from './lib/mat-input-fn.component';
export * from './lib/mat-input-fn.module';
