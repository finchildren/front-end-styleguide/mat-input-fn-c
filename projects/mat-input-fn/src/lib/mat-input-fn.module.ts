import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialCustomModule } from './modules/material.module';
import { MatInputFnComponent } from './mat-input-fn.component';



@NgModule({
  declarations: [
    MatInputFnComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MaterialCustomModule
  ],
  exports: [
    MatInputFnComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MatInputFnModule { }
