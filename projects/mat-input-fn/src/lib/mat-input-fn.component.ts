import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mat-input-fn',
  templateUrl: './mat-input-fn.component.html',
  styleUrls: ['./mat-input-fn.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MatInputFnComponent implements OnInit {

  @Input() label: string = 'Mat Input FN';

  constructor() { }

  ngOnInit(): void {

  }

}
