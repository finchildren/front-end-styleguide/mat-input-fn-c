import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatInputFnComponent } from './mat-input-fn.component';

describe('MatInputFnComponent', () => {
  let component: MatInputFnComponent;
  let fixture: ComponentFixture<MatInputFnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatInputFnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatInputFnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
