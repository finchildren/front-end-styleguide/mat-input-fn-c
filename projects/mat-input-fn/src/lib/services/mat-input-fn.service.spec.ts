import { TestBed } from '@angular/core/testing';

import { MatInputFnService } from './mat-input-fn.service';

describe('MatInputFnService', () => {
  let service: MatInputFnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatInputFnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
