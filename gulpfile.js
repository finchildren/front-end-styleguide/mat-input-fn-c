const gulp = require('gulp');
var run = require('gulp-run');


gulp.task('build', async function(cb) {
  run('npm run gulp-build').exec();
  cb();
});

gulp.task('server', function(cb) {
  setTimeout(() => {
    run('npm run gulp-serve').exec();
    cb();
  }, 10000);
});

exports.default = gulp.series('build','server');
